#/bin/bash
rostopic echo -b file.bag -p /mavros/local_position/pose > local_pos_est.csv
rostopic echo -b file.bag -p /mavros/vision_pose/pose > local_pos.csv
rostopic echo -b file.bag -p /mavros/local_position/velocity > local_vel.csv
rostopic echo -b file.bag -p /mavros/setpoint_position/local > set_pos.csv
rostopic echo -b file.bag -p /mavros/rc/out > rc.csv
rostopic echo -b file.bag -p /flynet/traj_gen > traj.csv

