This is the FLYNET SITL general directory. It includes useful files for getting SITL up and running as well as a few others:

bagger.sh -> File for processing saved ROS bag information. It pulls apart the recordings done by tuning_rec.sh into .csv files that can be processed in matlab using data_proc.m

cur_gain.txt -> Current settings of the PX4 firmware as of the last flights from the fall semester.

data_proc.m -> matlab file for visualizing recording results see bagger.sh and tuning_rec.sh

iris.sdf -> Modified iris model file, you'll need to replace the existing iris.sdf in the 
			Firmware/ directory use search commands to find the existing iris.sdf and replace 
			it with this modified file that contains quad physical paramteres for the 
			latest version of our Alien Bee quad. 
			
SITL_howto.txt -> It's the howto for setting up the SITL simulation and running our code. 

SITL_set.txt -> this is the current settings for the PX4 firwmare and where you'll adjust and set 
				control gains. 
				
tuning_rec.sh -> Run this script once everything else is running to record the relevant topics. 
