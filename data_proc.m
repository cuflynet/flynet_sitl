clear,clc%,close('all')
pos_dat = importdata('local_pos.csv',',',1); 
est_dat = importdata('local_pos_est.csv',',',1); 
vel_dat =  importdata('local_vel.csv',',',1); 
set_dat = importdata('set_pos.csv',',',1);
traj_dat = importdata('traj.csv',',',1);
pos_fs = 100; % H
set_fs = 75;
vis_fs = 1;
est_xyz = est_dat.data(:,1:3);
pos_xyz = pos_dat.data(:,1:3);
set_xyz = set_dat.data(:,1:3);
vel_xyz = vel_dat.data(:,1:3);
traj_xyz = traj_dat.data(:,2:3);
pos_time = 1:length(pos_xyz);
pos_time = pos_time.'/pos_fs;
traj_time = 1:length(traj_xyz);
traj_time = traj_time.'/pos_fs;
vel_time = 1:length(vel_xyz);
vel_time = vel_time.'/pos_fs;
set_time = 1:length(set_xyz);
set_time = set_time.'/set_fs;
figure(3)
clf(3)
subplot(2,2,1)
hold all
plot(pos_xyz(:,1),pos_xyz(:,2))
plot(est_xyz(:,1),est_xyz(:,2))
plot(traj_xyz(:,1),traj_xyz(:,2))
plot(set_xyz(:,1),set_xyz(:,2))
hold off
legend('Truth','Est','Com','WP')
% figure(2)
% clf(2)
xlabel('Top View')
subplot(2,2,2)
hold all
plot(pos_time,pos_xyz(:,1))
plot(set_time,set_xyz(:,1))
plot(traj_time,traj_xyz(:,1))
hold off
xlabel('X Com')
% figure(3)
% clf(3)
subplot(2,2,3)
hold all
plot(pos_time,pos_xyz(:,3))
plot(set_time,set_xyz(:,3))
ylim([0,3])
hold off
xlabel('Alt Hold')
% figure(4)
% clf(4)
subplot(2,2,4)
plot(vel_time,hypot(vel_xyz(:,1),vel_xyz(:,2)))
xlabel('Cart Velocity')